<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Socket</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
    <div class="wrapper">
        <div class="content-wrapper">
            <section class="content map_sec">
                <!-- Main row -->
                <div class="row">
                    <section class="col-lg-12">
                        <!-- Map box -->
                        <div id="map" style="height:95vh; width: 100%;"></div>
                    </section>
                </div>
            </section>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>
    <script>
        var delay = 10;

        busIconPath = "M392.002,103.161 L392.002,108.969 C392.002,108.969 390.500,109.171 388.673,109.434 C386.827,109.697 386.583,117.589 388.146,127.039 L390.987,144.360 C390.500,145.331 389.810,146.181 388.978,146.890 C385.244,136.853 382.707,111.498 382.707,83.634 C382.707,53.443 385.629,26.773 389.810,18.476 C390.277,18.982 390.683,19.548 390.987,20.176 L388.146,37.436 C386.583,46.907 386.827,54.798 388.673,55.061 C390.500,55.324 392.002,55.527 392.002,55.527 L392.002,61.415 L390.581,61.415 L390.581,103.161 L392.002,103.161 ZM381.489,83.634 C381.489,112.226 384.006,137.298 387.902,147.517 L388.470,147.294 C387.030,148.387 385.244,149.034 383.295,149.034 L64.940,149.034 L62.545,149.034 C62.543,149.034 62.542,149.034 62.541,149.034 L62.542,149.043 C62.542,149.043 61.150,148.926 58.862,148.728 L58.781,148.985 C58.786,148.993 58.793,149.000 58.798,149.008 C58.893,149.160 58.946,149.317 58.946,149.479 L58.946,161.635 C58.946,161.796 58.893,161.953 58.798,162.106 C58.737,162.203 58.635,162.295 58.543,162.388 C58.492,162.439 58.461,162.492 58.403,162.541 C58.249,162.670 58.064,162.791 57.865,162.908 C57.850,162.916 57.840,162.926 57.825,162.934 C56.703,163.576 55.028,164.001 54.199,164.001 C54.199,164.001 54.199,162.942 54.199,161.635 L54.199,149.479 C54.199,149.047 54.199,148.664 54.199,148.316 C42.955,147.310 23.594,145.474 17.109,144.218 C8.577,142.566 7.411,128.624 7.251,124.830 C4.135,122.853 1.852,119.277 1.662,115.667 C4.057,116.779 26.482,127.120 50.348,131.389 C50.085,131.086 49.841,130.762 49.658,130.398 C52.845,130.985 56.031,131.470 59.217,131.814 C59.115,129.932 59.014,127.909 58.912,125.784 C58.892,125.136 58.852,124.469 58.831,123.801 C58.811,123.295 58.791,122.749 58.770,122.223 C58.689,120.057 58.608,117.791 58.547,115.444 C58.527,114.230 58.486,112.975 58.466,111.700 C58.405,109.596 58.365,107.451 58.344,105.225 C58.304,103.546 58.283,101.825 58.283,100.065 C58.263,99.701 58.263,99.336 58.263,98.972 C58.243,96.847 58.222,94.702 58.222,92.517 C58.202,92.133 58.202,91.769 58.202,91.384 C58.202,89.887 58.202,88.389 58.182,86.892 L58.202,86.892 C58.182,86.102 58.182,85.313 58.182,84.524 L58.182,82.278 L58.182,80.032 L58.182,77.685 C58.202,76.167 58.202,74.669 58.202,73.192 C58.202,72.261 58.222,71.351 58.222,70.440 C58.222,69.530 58.222,68.619 58.243,67.729 C58.283,61.982 58.365,56.539 58.486,51.459 C58.527,50.185 58.547,48.910 58.588,47.675 L58.649,45.854 C58.669,45.045 58.689,44.256 58.730,43.467 C58.872,39.561 59.034,35.959 59.217,32.742 C57.451,32.944 55.665,33.167 53.900,33.430 C53.332,33.531 52.743,33.632 52.175,33.713 C51.546,33.814 50.917,33.936 50.267,34.057 C50.551,33.673 50.896,33.329 51.262,32.985 C27.071,37.173 4.077,47.777 1.662,48.910 C1.836,45.298 4.129,41.722 7.251,39.735 C7.411,35.939 8.578,22.001 17.109,20.350 C23.594,19.094 42.955,17.258 54.199,16.252 C54.199,15.819 54.199,15.211 54.199,14.521 L54.199,2.365 C54.199,1.058 54.199,-0.001 54.199,-0.001 C55.028,-0.001 56.703,0.424 57.826,1.066 C57.836,1.072 57.844,1.079 57.854,1.085 C58.058,1.203 58.246,1.327 58.403,1.459 C58.461,1.507 58.492,1.561 58.543,1.612 C58.635,1.705 58.737,1.796 58.798,1.894 C58.893,2.046 58.946,2.204 58.946,2.365 L58.946,14.521 C58.946,14.682 58.893,14.840 58.798,14.992 C58.793,15.000 58.786,15.007 58.781,15.014 L59.036,15.825 C61.146,15.642 62.429,15.534 62.519,15.527 C62.527,15.526 62.537,15.522 62.545,15.522 L64.940,15.522 L383.295,15.522 C385.650,15.522 387.801,16.472 389.363,17.990 L388.815,17.707 C384.513,25.902 381.489,53.018 381.489,83.634 ZM57.947,15.739 C57.918,15.756 57.895,15.776 57.866,15.794 C57.851,15.802 57.840,15.812 57.825,15.821 C57.743,15.868 57.651,15.909 57.564,15.953 C57.708,15.941 57.863,15.927 58.003,15.915 L57.947,15.739 ZM66.015,139.585 L381.347,139.585 C381.591,139.585 381.773,139.382 381.773,139.139 C381.773,138.897 381.591,138.714 381.347,138.714 L66.015,138.714 C65.792,138.714 65.589,138.897 65.589,139.139 C65.589,139.382 65.792,139.585 66.015,139.585 ZM340.008,95.593 L340.008,71.675 C340.008,67.365 336.497,63.864 332.174,63.864 L297.694,63.864 C293.372,63.864 289.861,67.365 289.861,71.675 L289.861,95.593 C289.861,99.903 293.372,103.424 297.694,103.424 L332.174,103.424 C336.497,103.424 340.008,99.903 340.008,95.593 ZM161.256,95.593 L161.256,71.675 C161.256,67.365 157.745,63.864 153.423,63.864 L118.943,63.864 C114.620,63.864 111.109,67.365 111.109,71.675 L111.109,95.593 C111.109,99.903 114.620,103.424 118.943,103.424 L153.423,103.424 C157.745,103.424 161.256,99.903 161.256,95.593 ZM381.347,24.992 L66.015,24.992 C65.792,24.992 65.589,25.174 65.589,25.417 C65.589,25.659 65.792,25.862 66.015,25.862 L381.347,25.862 C381.591,25.862 381.773,25.659 381.773,25.417 C381.773,25.174 381.591,24.992 381.347,24.992 ZM1.642,49.820 C1.723,49.820 1.784,49.800 1.845,49.780 C2.089,49.659 25.366,38.570 50.267,34.057 C49.861,34.583 49.537,35.170 49.334,35.818 C48.461,42.576 47.852,50.569 47.446,59.149 C46.716,64.876 46.208,70.724 45.944,76.693 C45.944,76.794 45.924,76.895 45.924,76.997 C45.782,79.870 45.701,82.764 45.681,85.677 C45.681,87.924 45.762,90.048 45.884,91.890 C46.147,95.836 46.614,98.628 47.162,99.094 C47.507,110.365 48.238,120.988 49.334,129.588 C49.415,129.872 49.537,130.135 49.658,130.398 C25.001,125.804 2.089,114.897 1.845,114.776 C1.784,114.736 1.703,114.736 1.642,114.736 L1.642,115.181 C1.642,115.343 1.642,115.504 1.662,115.667 L1.642,115.646 L1.642,117.670 C1.642,118.034 1.277,118.358 0.830,118.358 C0.364,118.358 -0.002,118.034 -0.002,117.670 L-0.002,46.907 C-0.002,46.522 0.364,46.219 0.830,46.219 C1.277,46.219 1.642,46.522 1.642,46.907 L1.642,48.930 C1.642,48.910 1.662,48.910 1.662,48.910 C1.642,49.072 1.642,49.234 1.642,49.396 L1.642,49.820 ZM389.039,147.072 L388.470,147.294 C388.653,147.173 388.815,147.031 388.978,146.890 C388.998,146.950 389.018,147.011 389.039,147.072 ZM389.911,18.273 C389.871,18.334 389.850,18.415 389.810,18.476 C389.688,18.314 389.526,18.152 389.363,17.990 L389.911,18.273 ZM391.129,20.459 C391.109,20.358 391.048,20.257 390.987,20.176 L390.987,20.135 C391.048,20.236 391.088,20.337 391.129,20.459 Z";

        $( document ).ready( function () {

            var socketUrl = 'localhost:3000';
			var socket = io( socketUrl );

			socket.on('connect', function (data) {
				console.log("Socket Connect Successfully.");
            });

            socket.on('disconnect', function() {
				console.log( 'disconnect' );
			});

			socket.on('reconnect', function() {
				console.log( 'reconect' );
				socket.connect();
				var data = JSON.parse('{ "webId" : 1 }');
				socket.emit('web_connect', data);
			});

			var data = JSON.parse('{ "webId" : 1 }');
            socket.emit('web_connect', data);

            socket.on('get_bus_location', function( data ) {

            });

            socket.on( 'get_bus_location', function (data) {

                var oldPos   = busMarker_1.getPosition();
                var point1   = new google.maps.LatLng( oldPos.lat(), oldPos.lng() );
                var point2   = new google.maps.LatLng( data.latitude, data.longitude );
                var heading  = google.maps.geometry.spherical.computeHeading( point1, point2 );

                LiveTracking( data.latitude, data.longitude, heading );

            });

        });

        function initMap(){

            bounds = new google.maps.LatLngBounds();

            map = new google.maps.Map( document.getElementById( 'map' ), {
                center: {
                    lat: 23.053177,
                    lng: 72.480210
                },
                mapTypeControl: false,
                zoomControl: true,
                streetViewControl: false,
                zoomControlOptions: {
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                zoom: 16
            } );

            var noPoi = [ {
                featureType: "poi",
                stylers: [ {
                    visibility: "off"
                } ]
            } ];

            map.setOptions( {
                styles: noPoi
            } );

            busIcon = {
                path: busIconPath,
                scale: 0.1,
                scaledSize: new google.maps.Size(20, 40), // scaled size
                anchor: new google.maps.Point(35,35), // anchor
				fillOpacity: 1,
				fillColor: "#FDD835",
				strokeColor:"#000000",
				strokeOpacity:1,
				strokeWeight:0.3
            };

			busMarker_1 = makeMarker( {
				lat: 23.053177,
				lng: 72.480210
			}, busIcon, 'Bus', map );

        }

        function makeMarker( position, icon, title, map ) {
            var marker = new google.maps.Marker( {
                position: position,
                map: map,
                icon: icon,
                title: title
            } );
            return marker;
        }

        function loadScript() {
            var script  = document.createElement( 'script' );
            script.type = 'text/javascript';
            script.src  = 'https://maps.googleapis.com/maps/api/js?' + 'key=' + getUrlParameter( 'key' ) +'&libraries=places&callback=initMap&libraries=geometry';
            document.body.appendChild( script );
        }

        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
        };

        window.onload = loadScript;

        var deltaLat_1, deltaLng_1, degreeFinal_1, numDeltas_1 = 500, i = 0;

        function LiveTracking( socket_lat, socket_lng, socket_angle ){

            var driver_previous_latitude  = busMarker_1.getPosition().lat();
            var driver_previous_longitude = busMarker_1.getPosition().lng();

            if( ( driver_previous_latitude == '' || driver_previous_latitude == null ) && ( driver_previous_longitude == '' || driver_previous_longitude == null ) ){
                driver_previous_latitude  = 0;
                driver_previous_longitude = 0;
            }

            if ( socket_angle > 0 ) {
                degreeFinal_1 = socket_angle;
            } else {
                degreeFinal_1 = bearingFinal( driver_previous_latitude, driver_previous_longitude, socket_lat, socket_lng );
            }

            transition( socket_lat, socket_lng, socket_angle );

        }

        function transition( socket_lat, socket_lng, socket_angle ) {

            var driverLocation = new google.maps.LatLng( socket_lat, socket_lng, socket_angle );

			var driver_previous_latitude = busMarker_1.getPosition().lat();
			var driver_previous_longitude = busMarker_1.getPosition().lng();

			i = 0;
			deltaLat_1 = ( socket_lat - driver_previous_latitude ) / numDeltas_1;
			deltaLng_1 = ( socket_lng - driver_previous_longitude ) / numDeltas_1;
			moveMarker();
        }

        function moveMarker(){

			var dp_lat  = busMarker_1.getPosition().lat();
            var dp_long = busMarker_1.getPosition().lng();

			if ( ( dp_lat == '' || dp_lat == null ) && ( dp_long == '' || dp_long == null ) )
			{
				dp_lat  = 0;
				dp_long = 0;
			}

			if ( degreeFinal_1 < 0 ) {
				degreeFinal_1 = bearingFinal( dp_lat, dp_long, socket_lat, socket_lng );
            }

			var bus1Icon      = busMarker_1.getIcon();
			bus1Icon.rotation = parseFloat( degreeFinal_1 ) + 90;
			busMarker_1.setIcon( bus1Icon );
			dp_lat            += deltaLat_1;
			dp_long           += deltaLng_1;
			var latlng        = new google.maps.LatLng( dp_lat, dp_long );
			busMarker_1.setPosition( latlng );

			if ( i != numDeltas_1 ) {
				i++;
				setTimeout( moveMarker, delay );
			}
		}

        function bearingFinal( lat1, long1, lat2, long2 ) {
			return ( bearingDegrees( lat2, long2, lat1, long1 ) + 180 ) % 360;
	    }

        function bearingDegrees( lat1, long1, lat2, long2 ) {

            var degToRad = Math.PI / 180.0;
            var phi1     = lat1 * degToRad;
            var phi2     = lat2 * degToRad;
            var lam1     = long1 * degToRad;
            var lam2     = long2 * degToRad;

            return Math.atan2( Math.sin( lam2 - lam1 ) * Math.cos( phi2 ), Math.cos( phi1 ) * Math.sin( phi2 ) - Math.sin( phi1 ) * Math.cos( phi2 ) * Math.cos( lam2 - lam1 ) ) * 180 / Math.PI;
        }

    </script>
</body>
</html>
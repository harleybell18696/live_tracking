var express = require('express');
var app     = express();
var server  = require('http').createServer();
var io      = require('socket.io')(server);

global.connectedDevice = {
    connectedWeb : [],
};

app.set( 'port', process.env.PORT || 3000 );

var General = require('./api/v1/socket/general');

var general = new General();

io.on('connection', function(socket) {

    var address = socket.handshake.address;
    console.log("user connected=======", address)

    socket.on('web_connect', function ( data ) {
        general.onWebSubscribe( socket, data );
    });

    setTimeout(() => {
        general.setBuslocation( io );
    }, 1000);


    socket.on('disconnect', function () {
       console.log('A user disconnected');
    });
});

server.listen( app.get( 'port' ) );
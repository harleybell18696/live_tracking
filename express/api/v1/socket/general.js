var globalConnectedArray = global.connectedDevice;

class General {

    onWebSubscribe( socket, data ){

        if( data.webId == 1 ){
            globalConnectedArray.connectedWeb.push( { socketId : socket.id, webId : 1 } );
        }

    }

    setBuslocation( io ){

        var location = [
            { latitude : 23.053177, longitude : 72.480210 },
            { latitude : 23.052744, longitude : 72.480032 },
            { latitude : 23.052290, longitude : 72.479902 },
            { latitude : 23.052051, longitude : 72.479837 },
            { latitude : 23.051656, longitude : 72.479808 },
            { latitude : 23.051118, longitude : 72.479770 },
            { latitude : 23.050580, longitude : 72.479773 },
            { latitude : 23.050232, longitude : 72.479801 },
            { latitude : 23.049636, longitude : 72.479850 },
            { latitude : 23.049172, longitude : 72.479893 },
            { latitude : 23.048708, longitude : 72.479957 },
            { latitude : 23.047376, longitude : 72.480103 },
            { latitude : 23.046675, longitude : 72.480167 },
            { latitude : 23.045846, longitude : 72.480231 },
            { latitude : 23.044750, longitude : 72.480295 },
            { latitude : 23.043447, longitude : 72.480145 },
            { latitude : 23.043131, longitude : 72.480081 },
            { latitude : 23.043200, longitude : 72.481379 },
            { latitude : 23.043328, longitude : 72.482377 },
            { latitude : 23.043121, longitude : 72.483761 },
            { latitude : 23.042775, longitude : 72.485381 },
            { latitude : 23.042617, longitude : 72.486186 },
            { latitude : 23.044098, longitude : 72.486422 },
            { latitude : 23.046950, longitude : 72.486782 },
            { latitude : 23.049625, longitude : 72.487125 },
            { latitude : 23.051027, longitude : 72.487297 },
            { latitude : 23.051580, longitude : 72.486986 },
            { latitude : 23.052000, longitude : 72.484869 },
            { latitude : 23.052395, longitude : 72.482455 },
            { latitude : 23.052780, longitude : 72.481125 },
            { latitude : 23.053177, longitude : 72.480210 }
        ];

        var i = 0;

        setInterval( function(){

            if( globalConnectedArray.connectedWeb.length > 0 ){

                for ( var n = 0; n < globalConnectedArray.connectedWeb.length; n++ ){

                    io.to( globalConnectedArray.connectedWeb[ n ].socketId ).emit( 'get_bus_location', location[ i ] );

                }

            }

            i++;

            if( i == ( location.length - 1 ) ) i = 0;

        }, 3000 );

    }

}

module.exports = General;